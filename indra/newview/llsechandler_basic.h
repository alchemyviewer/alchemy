/** 
 * @file llsechandler_basic.h
 * @brief Security API for services such as certificate handling
 * secure local storage, etc.
 *
 * $LicenseInfo:firstyear=2009&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#ifndef LLSECHANDLER_BASIC
#define LLSECHANDLER_BASIC

#include "llsecapi.h"

// LLSecAPIBasicCredential class
class LLSecAPIBasicCredential : public LLCredential
{
public:
	LLSecAPIBasicCredential(const std::string& grid) : LLCredential(grid) {} 
	virtual ~LLSecAPIBasicCredential() = default;
	// return a value representing the user id, used for server and voice
	// (could be guid, name in format "name_resident", whatever)
	std::string userID() const override;	
	
	// printible string identifying the credential.
	std::string asString() const override;
};

// LLSecAPIBasicHandler Class
// Interface handler class for the various security storage handlers.
class LLSecAPIBasicHandler : public LLSecAPIHandler
{
public:
	
	LLSecAPIBasicHandler(const std::string& protected_data_filename,
						 const std::string& legacy_password_path);
	LLSecAPIBasicHandler();
	
	void init() override;
	
	virtual ~LLSecAPIBasicHandler();

	
	
	// protectedData functions technically should be pretected or private,
	// they are not because of llsechandler_basic_test imlementation

	// persist data in a protected store
	void setProtectedData(const std::string& data_type,
								  const std::string& data_id,
								  const LLSD& data) override;
	
	// retrieve protected data
	LLSD getProtectedData(const std::string& data_type,
								  const std::string& data_id) override;
	
	// delete a protected data item from the store
	void deleteProtectedData(const std::string& data_type,
									 const std::string& data_id) override;

	// persist data in a protected store's map
	virtual void addToProtectedMap(const std::string& data_type,
								   const std::string& data_id,
								   const std::string& map_elem,
								   const LLSD& data);

	// remove data from protected store's map
	virtual void removeFromProtectedMap(const std::string& data_type,
										const std::string& data_id,
										const std::string& map_elem);

	// credential management routines

	LLPointer<LLCredential> createCredential(const std::string& grid,
													 const LLSD& identifier, 
													 const LLSD& authenticator) override;

	// load single credencial from default storage
	LLPointer<LLCredential> loadCredential(const std::string& grid) override;

	// save credencial to default storage
	void saveCredential(LLPointer<LLCredential> cred, bool save_authenticator) override;

	void deleteCredential(LLPointer<LLCredential> cred) override;

	// has map of credentials declared as specific storage
	virtual bool hasCredentialMap(const std::string& storage,
								  const std::string& grid);

	// returns true if map is empty or does not exist
	virtual bool emptyCredentialMap(const std::string& storage,
									const std::string& grid);

	// load map of credentials from specific storage
	virtual void loadCredentialMap(const std::string& storage,
								   const std::string& grid,
								   credential_map_t& credential_map);

	// load single username from map of credentials from specific storage
	virtual LLPointer<LLCredential> loadFromCredentialMap(const std::string& storage,
														  const std::string& grid,
														  const std::string& userid);

	// add item to map of credentials from specific storage
	virtual void addToCredentialMap(const std::string& storage,
									LLPointer<LLCredential> cred,
									bool save_authenticator);

	// remove item from map of credentials from specific storage
	virtual void removeFromCredentialMap(const std::string& storage,
										 LLPointer<LLCredential> cred);

	// remove item from map of credentials from specific storage
	virtual void removeFromCredentialMap(const std::string& storage,
										 const std::string& grid,
										 const std::string& userid);

	virtual void removeCredentialMap(const std::string& storage,
									 const std::string& grid);


protected:
	void _readProtectedData();
	void _writeProtectedData();
	std::string _legacyLoadPassword();

	std::string mProtectedDataFilename;
	LLSD mProtectedDataMap;
	
	std::string mLegacyPasswordPath;
};

#endif // LLSECHANDLER_BASIC



